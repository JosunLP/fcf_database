DROP DATABASE IF EXISTS `FlixChillFood`;
CREATE DATABASE IF NOT EXISTS `FlixChillFood` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `FlixChillFood`;




###########################################################
#Age_Set###################################################
###########################################################

CREATE TABLE FSK_Table (
	AID int NOT NULL AUTO_INCREMENT,
	FSK int,
	PRIMARY KEY (AID)
	);





###########################################################
#Genres####################################################
###########################################################

CREATE TABLE Genre (
	GID int NOT NULL AUTO_INCREMENT,
	Genre varchar(255),
	PRIMARY KEY (GID)
	);

#############################################################
#Merch#######################################################
#############################################################

CREATE TABLE Merch_Table (
	MEID int ,
	Merch varchar(255),
	MUrl varchar(9999),
	PRIMARY KEY (MEID)
	);

###########################################################
#Films_Series##############################################
###########################################################

CREATE TABLE Films (
	FID int NOT NULL AUTO_INCREMENT,
	FTitle varchar(255),
	Film_Link varchar(9999),
	GID int,
	AID int,
	MEID int,
	PRIMARY KEY (FID),
	CONSTRAINT FK_GID FOREIGN KEY (GID) REFERENCES Genre(GID),
	CONSTRAINT FK_AID FOREIGN KEY (AID) REFERENCES FSK_Table(AID),
	CONSTRAINT FK_MEID FOREIGN KEY (MEID) REFERENCES Merch_Table(MEID)
	);

CREATE TABLE Series (
	SID int NOT NULL AUTO_INCREMENT,
	STitle varchar(255),
	S_Link varchar(9999),
	GID int,
	AID int,
	MEID int,
	PRIMARY KEY (SID),
	CONSTRAINT FK_GIDs FOREIGN KEY (GID) REFERENCES Genre(GID),
	CONSTRAINT FK_AIDs FOREIGN KEY (AID) REFERENCES FSK_Table(AID),
	CONSTRAINT FK_MEIDs FOREIGN KEY (MEID) REFERENCES Merch_Table(MEID)
	);


############################################################
#Food_Snacks_and_Drinks#####################################
############################################################

CREATE TABLE Food (
	FOID int NOT NULL AUTO_INCREMENT,
	Food varchar(255),
	AID int,
	PRIMARY KEY (FOID),
	CONSTRAINT FK_AID_F FOREIGN KEY (AID) REFERENCES FSK_Table(AID)

	);
CREATE TABLE Drinks (
	DID int NOT NULL AUTO_INCREMENT,
	Drink varchar(255),
	AID int,
	PRIMARY KEY (DID),
	CONSTRAINT FK_AID_D FOREIGN KEY (AID) REFERENCES FSK_Table(AID)

	);
CREATE TABLE Snacks (
	SNID int NOT NULL AUTO_INCREMENT,
	Snack varchar (255),
	AID int,
	PRIMARY KEY (SNID),
	CONSTRAINT FK_AID_S FOREIGN KEY (AID) REFERENCES FSK_Table(AID)

	);

	
###########################################################
#Generator#################################################
###########################################################

CREATE TABLE Generator (
	GENID int NOT NULL AUTO_INCREMENT,
	FID int,
	FOID int,
	DID int,
	MEID int,
	PRIMARY KEY (GENID),
	CONSTRAINT FK_FID_G FOREIGN KEY (FID) REFERENCES Films(FID),
	CONSTRAINT FK_FOID_G FOREIGN KEY (FOID) REFERENCES Food(FOID),
	CONSTRAINT FK_DID_G FOREIGN KEY (DID) REFERENCES Drinks(DID),
	CONSTRAINT FK_MEID_G FOREIGN KEY (MEID) REFERENCES Merch_Table(MEID)
	);
	
###########################################################
#Users#####################################################
###########################################################

CREATE TABLE Users (
	USID int NOT NULL AUTO_INCREMENT,
	Username varchar (255),
	Firstname varchar (255),
	Lastname varchar (255),
	GENID int,
	AID int,
	PRIMARY KEY (USID),
	CONSTRAINT FK_AID_U FOREIGN KEY (AID) REFERENCES FSK_Table(AID),
	CONSTRAINT FK_GENID_U FOREIGN KEY (GENID) REFERENCES Generator(GENID)

	);


CREATE TABLE passw (
	PWID int NOT NULL AUTO_INCREMENT,
	PSSWD varchar (255),
	USID int,
	PRIMARY KEY (PWID),
	CONSTRAINT FK_USID_P FOREIGN KEY (USID) REFERENCES Users(USID)

	);