#SELECT ABFRAGEN


SELECT Username AS "Vorname", Lastname AS "Nachname", films.FTitle AS "Letzter Film", genre.Genre AS "Film Genre"
FROM users
	LEFT JOIN generator ON users.GENID = generator.GENID
	LEFT JOIN films ON generator.FID = films.FID
	LEFT JOIN genre ON films.GID = genre.GID
;

#Statement wirft zum Augenblicklichen Zeitpunkt nur NULL bei Letzter Film und Film Genre aufgrund fehlender Präferenzen im Generator.
#Diese sollen Variabel durch den Generator gesetzt werden


SELECT films.FTitle ,genre.Genre AS "Film Genre", merch_table.Merch
FROM films
	RIGHT JOIN genre ON films.GID = genre.GID
	LEFT JOIN merch_table ON films.MEID = merch_table.MEID
ORDER BY genre.GID DESC
;

#Abfrage über Filme und den zugehörigen Merch


SELECT users.Firstname AS Vorname, users.Lastname AS Nachname, passw.PSSWD AS 'Passwort'
FROM users
	INNER JOIN passw ON users.USID = passw.USID
;

#User Daten Abfrage für identifikation und Login abgleich


SELECT fsk_table.FSK AS "Alter", snacks.Snack AS "Snack", drinks.Drink AS "Drink"
FROM fsk_table
	JOIN snacks ON fsk_table.AID = snacks.AID
	JOIN drinks ON snacks.AID = drinks.AID
ORDER BY Snack ASC
;

#Nasch voschläge, orientiert am Alter



SELECT films.FTitle AS "Film Titel", films.Film_Link AS "Film Link", fsk_table.FSK
FROM films
	LEFT JOIN fsk_table ON films.AID = fsk_table.AID
ORDER BY FSK DESC


#Abfrage der Filme mit Alter


